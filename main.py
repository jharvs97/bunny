import random
from copy import copy

class Logger:
    def __init__(self):
        self.logs = []

    def log(self, message):
        self.logs.append(message)

    def write_to_disk(self):
        with open('./log.txt', 'w+') as f:
            for log in self.logs:
                f.write(log + '\n')

class Holes:
    def __init__(self, glorious_hole):
        self.logger = Logger()
        self.num_holes = 100
        self.bunny = glorious_hole
        self.guesses = 0
        self.logger.log("Glorious bunny hole @ " + str(glorious_hole))
    
    def __len__(self):
        return self.num_holes

    def guess(self, index):
        self.logger.log("Guess at " + str(index))
        before_hop = copy(self.bunny)

        if abs(self.bunny - index) == 1:
            self.hop()

        self.guesses += 1

        found = before_hop == index

        if found:
            self.logger.log("Found bunny at " + str(index) + ":3c yummie")
        else:
            self.logger.log("Missed bunny T_T (bunny is at " + str(self.bunny) + ") you suck")
            
        return found

    def hop(self):
        coin_flip = random.uniform(0.0, 1.0)
        self.logger.log("Coin flipped a " + str(coin_flip))

        move = 0
        if self.bunny == 0:
            move = 1
        elif self.bunny == self.num_holes - 1:
            move = -1
        else:
            if coin_flip > 0.5:
                move = 1
            else:
                move = -1

        self.logger.log("Hopping to the " + ( "right" if move == 1 else "left"))
        self.bunny += move

def double_tap(holes: Holes):
    found = False

    for i in range(len(holes)):
        if holes.guess(i):
            found = True
            break
        if holes.guess(i):
            found = True
            break
        i += 1

    holes.logger.write_to_disk()

    if found:
        print("found bunny at " + str(i) + " in " + str(holes.guesses) + " guesses")
    else:
        print("Not found")

def stealth_bomber(holes: Holes):
    found = False

    for k in range(2):
        for i in range(0, len(holes), 2):
            if holes.guess(i):
                found = True
                break

    holes.logger.write_to_disk()

    if found:
        print("found bunny at " + str(i) + " in " + str(holes.guesses) + " guesses")
    else:
        print("Not found")

def rain_of_arrows(holes: Holes):
    found = False

    for i in range(2, len(holes), 4):
        if holes.guess(i):
            found = True; break

    for i in range(0, len(holes), 2):
        if holes.guess(i):
            found = True; break

    holes.logger.write_to_disk()

    if found:
        print("found bunny at " + str(i) + " in " + str(holes.guesses) + " guesses")
    else:
        print("Not found")

def main():
    random.seed()
    glorious_hole_index = 0
    loops = 100
    guesses_total = 0
    watermark = 0

    while (glorious_hole_index <= loops - 1):
        holes = Holes(glorious_hole_index)
        stealth_bomber(holes)
        glorious_hole_index += 1

        guesses_total += holes.guesses
        if holes.guesses > watermark:
            watermark = holes.guesses

    average_number_of_guesses_that_this_algorithm_made_before_its_fat_ass_found_the_bunny_wabbit = guesses_total / loops
    print("\nAverage guesses: d(/\/| " + str(average_number_of_guesses_that_this_algorithm_made_before_its_fat_ass_found_the_bunny_wabbit) + " |\/\)b\n")
    print("\nWatermark: d(/\/| " + str(watermark) + " |\/\)b\n")

if __name__ == "__main__":
    main()